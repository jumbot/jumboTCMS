﻿/*
 * 程序名称: JumboTCMS(将博内容管理系统通用版)
 * 
 * 程序版本: 7.x
 * 
 * 程序作者: 子木将博 (QQ：791104444@qq.com，仅限商业合作)
 * 
 * 版权申明: http://www.jumbotcms.net/about/copyright.html
 * 
 * 技术答疑: http://forum.jumbotcms.net/
 * 
 */



using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace JumboTCMS.WebFile.User
{
    public partial class _Download : JumboTCMS.UI.UserCenter
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string _filename = q("if_filename");
            string _title = q("if_title");
            string _md5 = q("md5");
            User_Load("", "html");
            if (_md5 == JumboTCMS.Utils.MD5.Upper32(_filename + "_" + _title))
            {
                DownloadFile(_filename, _title,0);
            }
            else
            {
                Response.Write("MD5验证有误");
                Response.End();
            }

        }
    }
}