﻿/*
 * 程序名称: JumboTCMS(将博内容管理系统通用版)
 * 
 * 程序版本: 7.x
 * 
 * 程序作者: 子木将博 (QQ：791104444@qq.com，仅限商业合作)
 * 
 * 版权申明: http://www.jumbotcms.net/about/copyright.html
 * 
 * 技术答疑: http://forum.jumbotcms.net/
 * 
 */





using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JumboTCMS.WebFile.Admin
{
    public partial class _crop_avatar_index : JumboTCMS.UI.AdminCenter
    {
        public int ImgWidth = 0;
        public int ImgHeight = 0;
        public int ImgWidth_s = 180;
        public int ImgHeight_s = 0;
        public bool HasImg = false;
        public string Img = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            ChannelId = Str2Str(q("ccid"));
            Admin_Load("", "html", true);
            Img = q("photo") == "" ? "crop_avatar/img/blank.jpg" : q("photo");
            HasImg = q("photo") == "" ? false : true;
            doh.Reset();
            doh.ConditionExpress = "Id=(select DefaultThumbs from jcms_normal_channel where id=" + ChannelId+")";
            object[] value = doh.GetFields("jcms_normal_thumbs", "iWidth,iHeight");
            if (value == null)
            {
                Response.Write("参数有误");
            }
            else
            {
                ImgWidth = Str2Int(value[0].ToString());
                ImgHeight = Str2Int(value[1].ToString());
                ImgHeight_s = Convert.ToInt32(ImgHeight * 180 / ImgWidth);
            }


        }
    }
}