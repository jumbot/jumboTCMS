﻿/*
 * 程序名称: JumboTCMS(将博内容管理系统通用版)
 * 
 * 程序版本: 7.x
 * 
 * 程序作者: 子木将博 (QQ：791104444@qq.com，仅限商业合作)
 * 
 * 版权申明: http://www.jumbotcms.net/about/copyright.html
 * 
 * 技术答疑: http://forum.jumbotcms.net/
 * 
 */




using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
namespace JumboTCMS.WebFile.Admin
{
    public class _crop_avatar_ajax_ImgSize
    {
        //{"x":30.003846153846148,"y":16.715384615384625,"height":300.8,"width":300.8,"rotate":0}
        public double x { get; set; }
        public double y { get; set; }
        public double width { get; set; }
        public double height { get; set; }
        public double to_width { get; set; }
        public double to_height { get; set; }
        public int rotate { get; set; }

        public int ToInt(double doubleValue)
        {
            return Convert.ToInt32(doubleValue);
        }
    }
    public partial class _crop_avatar_ajax : JumboTCMS.UI.AdminCenter
    {
        private string _sAdminUploadPath = "/_data/tempfiles";
        private string _sAdminUploadType = "*.jpg;*.jpeg;*.bmp;*.gif;*.png;";
        private int _sAdminUploadSize = 2048;
        protected void Page_Load(object sender, EventArgs e)
        {
            ChannelId = Str2Str(q("ccid"));
            Admin_Load("", "json", true);

            HttpPostedFile file = Request.Files["avatar_file"];
            string _avatar_src = f("avatar_src");//不为空的话传的是URL地址
            string datasize = Request.Params["avatar_data"];
            JavaScriptSerializer jss = new JavaScriptSerializer();
            _crop_avatar_ajax_ImgSize imagesize = jss.Deserialize<_crop_avatar_ajax_ImgSize>(datasize);



            Bitmap sBitmap = null;

            if (_avatar_src != "")
                sBitmap = (Bitmap)Image.FromFile(Server.MapPath(_avatar_src));
            else
            {
                byte[] FileByte = SetFileToByteArray(file);//图片数组
                string strtExtension = System.IO.Path.GetExtension(file.FileName);//图片格式
                MemoryStream ms1 = new MemoryStream(FileByte);
                sBitmap = (Bitmap)Image.FromStream(ms1);
            }
            Rectangle section = new Rectangle(new Point(imagesize.ToInt(imagesize.x), imagesize.ToInt(imagesize.y)), new Size(imagesize.ToInt(imagesize.width), imagesize.ToInt(imagesize.height)));

            string fileExtension = ".jpg";//缩略图后缀名
            string DirectoryPath = ChannelUploadPath + DateTime.Now.ToString("yyMMdd");

            JumboTCMS.Utils.DirFile.CreateDir(DirectoryPath);

            string sFileName = DateTime.Now.ToString("yyyyMMddHHmmssffff") + "_thumb" + fileExtension;  // 文件名称
            string thumbnailPath = DirectoryPath + "/" + sFileName;        // 服务器端文件路径
            if (MakeThumbnailImage(sBitmap, Server.MapPath(thumbnailPath), imagesize.ToInt(imagesize.to_width), imagesize.ToInt(imagesize.to_height), section.Width, section.Height, section.X, section.Y))
            {
                JObject _jo = new JObject();
                _jo.Add("state", 200);
                _jo.Add("result", thumbnailPath);
                Response.Write(_jo.ToString());
            }
            else
            {
                JObject _jo = new JObject();
                _jo.Add("state", 200);
                _jo.Add("message", "制作失败");
                Response.Write(_jo.ToString());
            }
        }
        /// <summary>
        /// 将From表单file文件转化为byte数组
        /// </summary>
        /// <param name="File">from提交文件流</param>
        /// <returns></returns>
        private byte[] SetFileToByteArray(HttpPostedFile File)
        {
            Stream stream = File.InputStream;
            byte[] AyyayByte = new byte[File.ContentLength];
            stream.Read(AyyayByte, 0, File.ContentLength);
            stream.Close();
            return AyyayByte;
        }
        /// <summary>
        /// 裁剪图片并保存
        /// </summary>
        /// <param name="Image">图片信息</param>
        /// <param name="maxWidth">缩略图宽度</param>
        /// <param name="maxHeight">缩略图高度</param>
        /// <param name="cropWidth">裁剪宽度</param>
        /// <param name="cropHeight">裁剪高度</param>
        /// <param name="X">X轴</param>
        /// <param name="Y">Y轴</param>
        public static bool MakeThumbnailImage(Image originalImage,string thumbnailPath, int maxWidth, int maxHeight, int cropWidth, int cropHeight, int X, int Y)
        {
            Bitmap b = new Bitmap(cropWidth, cropHeight);
            try
            {
                using (Graphics g = Graphics.FromImage(b))
                {
                    //清空画布并以透明背景色填充
                    g.Clear(Color.Transparent);
                    //在指定位置并且按指定大小绘制原图片的指定部分
                    g.DrawImage(originalImage, new Rectangle(0, 0, cropWidth, cropHeight), X, Y, cropWidth, cropHeight, GraphicsUnit.Pixel);
                    Image displayImage = new Bitmap(b, maxWidth, maxHeight);
                    displayImage.Save(thumbnailPath, System.Drawing.Imaging.ImageFormat.Jpeg);
                    Bitmap bit = new Bitmap(b, maxWidth, maxHeight);

                }
                originalImage.Dispose();
                b.Dispose();
                return true;
            }
            catch (System.Exception e)
            {
                originalImage.Dispose();
                b.Dispose();
                return false;
            }
        }
    }
}