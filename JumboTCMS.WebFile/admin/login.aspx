﻿<%@ Page Language="C#" AutoEventWireup="True" Codebehind="login.aspx.cs" Inherits="JumboTCMS.WebFile.Admin._login" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>管理中心 - Powered By JumboTCMS</title>
<link type="text/css" href="/statics/admin/login/css/style.css" rel="stylesheet" />
<script type="text/javascript" src="/_libs/jquery.tools.pack.js?ver=20200909"></script>
<script type="text/javascript" src="/_data/global.js"></script>

<script type="text/javascript">    if (top != self) top.location = self.location;</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#loginform input').bind('keypress', function (event) {
            if (event.keyCode == "13") {
                ajaxLogin(); ;
            }
        });
    });

    function ajaxLogin() {
        var uName = $('#txtAdminName').val();
        if (uName == "") {
            $('#errorInfo').text('请输入账号');
            return;
        }
        var uPass = $('#txtAdminPass').val();
        if (uPass == "") {
            $('#errorInfo').text('请输入密码');
            return;
        }
        uPass = JumboTCMS.MD5(uPass);
        var cookieday = 0;
        if ($('#savepass').is(':checked')) {
            cookieday = 7;
        }
        $.ajax({
            type: "post",
            dataType: "html",
            url: "ajax.aspx?oper=login&clienttime=" + Math.random(),
            data: "name=" + uName + "&pass=" + encodeURIComponent(uPass) + "&type=" + cookieday,
            error: function (XmlHttpRequest, textStatus, errorThrown) { alert("网络堵塞,稍后再试"); },
            success: function (d) {
                if (d == "ok")
                    top.location.href = 'default.aspx';
                else
                    $('#errorInfo').text(d);
            }
        });
    }

    function sendRequest(p) {
        if (p == 0) {
            return;
        }
        $.ajax({
            url: "ajax.aspx?clienttime=" + Math.random(),
            type: "get",
            dataType: "json",
            error: function (XmlHttpRequest, textStatus, errorThrown) { alert(XmlHttpRequest.responseText); },
            success: function (d) {
                if (d.result == "1")
                    top.location.href = 'default.aspx';
            }
        });
    }
    sendRequest(1);
</script>
</head>
<body>
    <div class="admin_zx">
    	<div id="loginform" class="admin_login">
        	<input type="text" maxlength="18" id="txtAdminName" class="input_id" placeholder="账号" />
            <input type="password" id="txtAdminPass" class="input_pwd" placeholder="密码" />
            <p class="cookieday"><input id="savepass" type="checkbox" /><label for="savepass">记住密码</label></p>
            <input type="button" value="" id="btnLogin" class="btn_login" onclick="ajaxLogin()" />
             <p id="errorInfo"></p>
        </div>
    </div>
</body>
</html>

