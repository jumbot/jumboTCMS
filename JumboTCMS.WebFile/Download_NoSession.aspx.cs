﻿/*
 * 程序名称: JumboTCMS(将博内容管理系统通用版)
 * 
 * 程序版本: 7.x
 * 
 * 程序作者: 子木将博 (QQ：791104444@qq.com，仅限商业合作)
 * 
 * 版权申明: http://www.jumbotcms.net/about/copyright.html
 * 
 * 技术答疑: http://forum.jumbotcms.net/
 * 
 */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JumboTCMS.WebFile
{
    public partial class Download_NoSession : System.Web.UI.Page
    {
        public static bool Download_Block = true;
        protected void Page_Load(object sender, EventArgs e)
        {
            string _filename = q("if_filename");
            string _title = q("if_title");
            string _download_advanced = q("download_advanced");
            string _datetime = q("datetime");
            string _sn = q("sn");
            string _datetime_right = System.DateTime.Now.ToString("yyyyMMddHH");
            string _sn_right = JumboTCMS.Utils.MD5.Lower32(_filename + "|" + _title + "|" + _download_advanced + "|" + _datetime);

            if (_datetime != _datetime_right)
            {
                Response.Write("链接已失效");
                Response.End();
            }

            if (_sn != _sn_right)
            {
                Response.Write("信息验证没通过");
                Response.End();
            }
            if (JumboTCMS.Utils.DirFile.FileExists(_filename))
            {

                Download_Block = _download_advanced == "0";
                JumboTCMS.Utils.Logs.Info("DownloadFile" + _download_advanced, Server.MapPath(_filename), false);
                DownloadFile(Server.MapPath(_filename), _title);
            }
            else
            {
                JumboTCMS.Utils.Logs.Error("FileExists", Server.MapPath(_filename),false);
                Response.Write("文件" + Server.MapPath(_filename) + "不存在，请联系：010-68186991");
            }
        }
        public  void DownloadFile(string _fullPath, string _fileName)
        {
            if (_fileName == "")
                _fileName = JumboTCMS.Utils.DirFile.GetFileName(false, _fullPath);
            string _fileExt = JumboTCMS.Utils.DirFile.GetFileExt(_fullPath);
            if (!_fileName.EndsWith("." + _fileExt))
                _fileName = _fileName + "." + _fileExt;

            if (Download_Block)
            {
                #region 分块下载
                System.IO.Stream iStream = null;
                int dataBufferSize = 102400;
                byte[] buffer = new Byte[dataBufferSize];
                int length;//文件大小
                long dataToRead;

                try
                {
                    iStream = new System.IO.FileStream(_fullPath, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read);
                    dataToRead = iStream.Length;
                    Response.Clear();
                    Response.ClearHeaders();
                    Response.ClearContent();
                    Response.AddHeader("Content-Length", dataToRead.ToString());
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + System.Web.HttpUtility.UrlEncode(_fileName.Replace(" ", "nbsp"), System.Text.Encoding.UTF8).Replace("nbsp", " "));

                    while (dataToRead > 0)
                    {
                        if (Response.IsClientConnected)
                        {
                            length = iStream.Read(buffer, 0, dataBufferSize);
                            Response.OutputStream.Write(buffer, 0, length);
                            Response.Flush();
                            buffer = new Byte[dataBufferSize];
                            dataToRead = dataToRead - length;
                        }
                        else
                        {
                            dataToRead = -1;
                        }
                    }
                    if (iStream != null)
                    {
                        iStream.Close();
                    }
                    Response.End();
                }
                catch (Exception ex)
                {
                    JumboTCMS.Utils.Logs.Error("download", _fullPath + "\r\n" + ex, false);
                    if (iStream != null)
                    {
                        iStream.Close();
                    }
                    Response.End();
                }
                #endregion
            }
            else
            {
                #region 不分块下载
                try
                {
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + System.Web.HttpUtility.UrlEncode(_fileName.Replace(" ", "nbsp"), System.Text.Encoding.UTF8).Replace("nbsp", " "));
                    Response.TransmitFile(_fullPath);
                }
                catch (Exception ex)
                {
                    JumboTCMS.Utils.Logs.Error("download", _fullPath + "\r\n" + ex, false);
                    Response.End();
                }
                #endregion
            }
        }
   
        /// <summary>
        /// 获取querystring
        /// </summary>
        /// <param name="s">参数名</param>
        /// <returns>返回值</returns>
        public string q(string s)
        {
            if (HttpContext.Current.Request.QueryString[s] != null && HttpContext.Current.Request.QueryString[s] != "")
            {
                return JumboTCMS.Utils.Strings.SafetyQueryS(HttpContext.Current.Request.QueryString[s].ToString());
            }
            return string.Empty;
        }
    }
}