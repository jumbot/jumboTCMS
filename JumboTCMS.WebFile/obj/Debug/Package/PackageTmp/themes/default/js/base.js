var _download_module = "soft";
var _download_n = "0";
function ConfirmDownload(module, ChannelId, id, n) {
    _download_module = module;
   _download_n = n;
    ajaxConfirmDownload(ChannelId, id)
}

function ajaxConfirmDownload(ChannelId, id) {
    $.ajax({
        type: "get",
        dataType: "json",
        data: "id=" + id + "&ChannelId=" + ChannelId + "&ChannelType=" + _download_module + "&n=" + _download_n,
        url: "/plus/ajax_user.aspx?oper=ajaxConfirmDownload&clienttime=" + Math.random(),
        error: function (XmlHttpRequest, textStatus, errorThrown) { if (XmlHttpRequest.responseText != "") alert('服务器忙，请重试'); },
        success: function (d) {
            switch (d.result) {
                case '-1':
                case '0': //未支付，返回当前剩余积分
                    alert(d.returnval);
                    break;
                case '1': //其实已经支付成功，直接跳转
                    location.href = '/user/download.aspx?if_filename=' + encodeURIComponent(d.body.filename) + '&if_title=' + encodeURIComponent(d.body.title) + '&md5=' + d.body.md5;
                    break;
                case '2': //直接跳转吧
                    location.href = d.body.url;
                    break;
            }
        }
    });
}