﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="crop_avatar_index.aspx.cs" Inherits="JumboTCMS.WebFile.Admin._crop_avatar_index" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>制作缩略图</title>
<link href="/_libs/cropper/assets/css/bootstrap.min.css" rel="stylesheet">
<link href="/_libs/cropper/dist/cropper.min.css" rel="stylesheet">
<style>

body {
  background-color: #fcfcfc;
}

.avatar-view {
  display: block;
  margin: 15px auto;
  width:<%=ImgWidth %>px;
  height:<%=ImgHeight%>px;
  border: 3px solid #fff;
  border-radius: 5px;
  box-shadow: 0 0 5px rgba(0,0,0,.15);
  cursor: pointer;
  overflow: hidden;
}

.avatar-view img {
  width: 100%;
}

.avatar-body {
  padding-right: 15px;
  padding-left: 15px;
}

.avatar-upload {
  overflow: hidden;
}

.avatar-upload label {
  display: block;
  float: left;
  clear: left;
  width: 100px;
}

.avatar-upload input {
  display: block;
  margin-left: 110px;
}

.avater-alert {
  margin-top: 10px;
  margin-bottom: 10px;
}

.avatar-wrapper {
  height: 364px;
  width: 100%;
  margin-top: 15px;
  box-shadow: inset 0 0 5px rgba(0,0,0,.25);
  background-color: #fcfcfc;
  overflow: hidden;
}

.avatar-wrapper img {
  display: block;
  height: auto;
  max-width: 100%;
}

.avatar-preview {
  float: left;
  margin-top: 15px;
  margin-right: 15px;
  border: 1px solid #eee;
  border-radius: 4px;
  background-color: #fff;
  overflow: hidden;
}

.avatar-preview:hover {
  border-color: #ccf;
  box-shadow: 0 0 5px rgba(0,0,0,.15);
}

.avatar-preview img {
  width: 100%;
}

.preview-lg {
  width:<%=ImgWidth_s%>px;
  height:<%=ImgHeight_s%>px;
  margin-top: 15px;
}

.preview-md {
  height: 100px;
  width: 100px;
}

.preview-sm {
  height: 50px;
  width: 50px;
}

@media (min-width: 992px) {
  .avatar-preview {
    float: none;
  }
}

.avatar-btns {
  margin-top: 30px;
  margin-bottom: 15px;
}

.avatar-btns .btn-group {
  margin-right: 5px;
}

.loading {
  display: none;
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  background: #fff url("crop_avatar/img/loading.gif") no-repeat center center;
  opacity: .75;
  filter: alpha(opacity=75);
  z-index: 20140628;
}

</style>
</head>
<body>
<div class="container" id="crop-avatar">
  <!-- Current avatar -->
  <div class="avatar-view hide" title="点击修改缩略图"> <img src="<%=Img %>" alt="当前缩略图"></div>
  <div class="row hide" style="margin:0 auto;width:180px;">
    <button class="btn btn-block avatar-view2" type="button">点击图片修改缩略图</button>
  </div>
  <!-- Cropping modal -->
  <div class="modal0 fade0" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog0" tabindex="-1">
    <div class="modal-dialog modal-lg" style="width:100%;margin-top:10px;">
      <div class="modal-content">
        <form class="avatar-form" action="crop_avatar_ajax.aspx?ccid=<%=ChannelId %>" enctype="multipart/form-data" method="post">
          <div class="modal-header hide">
            <button class="close hide" data-dismiss="modal" type="button">&times;</button>
            <h4 class="modal-title" id="avatar-modal-label">在线制作缩略图</h4>
          </div>
          <div class="modal-body">
            <div class="avatar-body">
              <!-- Upload image and data -->
              <div class="avatar-upload">
                <input class="avatar-src" name="avatar_src" type="hidden" value="<%=Img %>">
                <input class="avatar-data" name="avatar_data" type="hidden">
                <label for="avatarInput">本地上传</label>
                <input class="avatar-input" id="avatarInput" name="avatar_file" type="file">
              </div>
              <!-- Crop and preview -->
              <div class="row">
                <div class="col-md-9">
                  <div class="avatar-wrapper"></div>
                </div>
                <div class="col-md-3">
                  <div class="avatar-preview preview-lg"></div>
                  <div class="avatar-preview preview-md" style="display:none;"></div>
                  <div class="avatar-preview preview-sm" style="display:none;"></div>

                </div>
              </div>
              <!--<div class="row avatar-btns">
                <div class="col-md-9">
                  <div class="btn-group">
                    <button class="btn btn-primary" data-method="rotate" data-option="-90" type="button" title="Rotate -90 degrees">左旋转</button>
                    <button class="btn btn-primary" data-method="rotate" data-option="-15" type="button">-15°</button>
                    <button class="btn btn-primary" data-method="rotate" data-option="-30" type="button">-30°</button>
                    <button class="btn btn-primary" data-method="rotate" data-option="-45" type="button">-45°</button>
                  </div>
                  <div class="btn-group">
                    <button class="btn btn-primary" data-method="rotate" data-option="90" type="button" title="Rotate 90 degrees">右旋转</button>
                    <button class="btn btn-primary" data-method="rotate" data-option="15" type="button">15°</button>
                    <button class="btn btn-primary" data-method="rotate" data-option="30" type="button">30°</button>
                    <button class="btn btn-primary" data-method="rotate" data-option="45" type="button">45°</button>
                  </div>
                </div>
                <div class="col-md-3">
                  <button class="btn btn-primary btn-block avatar-save" type="submit">确定</button>
                </div>
              </div>-->
            </div>
          </div>
          <div class="modal-footer">
              <!--<button class="btn btn-default" data-dismiss="modal" type="button">关闭</button>-->
              <div class="col-md-9 text-left " style="color:red;">
                    操作小指南：①按住鼠标可左键拖拽/拉伸裁剪框；②鼠标滚轴可对图片进行缩放
              </div>
              <div class="col-md-3">
                  <button class="btn btn-primary avatar-save" type="submit">确定</button>
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>
  <!-- /.modal -->
  <!-- Loading state -->
  <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
</div>
<script src="/_libs/cropper/assets/js/jquery.min.js"></script>
<script src="/_libs/cropper/assets/js/bootstrap.min.js"></script>
<script src="/_libs/cropper/dist/cropper.min.js"></script>
<script type="text/javascript" src="/_data/global.js?ver=<%=site.Version %>"></script>
<script type="text/javascript">_jcms_SetDialogTitle();</script>
<script>
var ImgWidth = <%=ImgWidth%>;
var ImgHeight = <%=ImgHeight%>;
var HasImg = <%=HasImg?"true":"false"%>;
</script>
<script>
    (function (factory) {
        if (typeof define === 'function' && define.amd) {
            define(['jquery'], factory);
        } else if (typeof exports === 'object') {
            // Node / CommonJS
            factory(require('jquery'));
        } else {
            factory(jQuery);
        }
    })(function ($) {

        'use strict';

        var console = window.console || { log: function () { } };

        function CropAvatar($element) {
            this.$container = $element;

            this.$avatarView = this.$container.find('.avatar-view');
            this.$avatar = this.$avatarView.find('img');
            this.$avatarModal = this.$container.find('#avatar-modal');
            this.$loading = this.$container.find('.loading');

            this.$avatarForm = this.$avatarModal.find('.avatar-form');
            this.$avatarUpload = this.$avatarForm.find('.avatar-upload');
            this.$avatarSrc = this.$avatarForm.find('.avatar-src');
            this.$avatarData = this.$avatarForm.find('.avatar-data');
            this.$avatarInput = this.$avatarForm.find('.avatar-input');
            this.$avatarSave = this.$avatarForm.find('.avatar-save');
            this.$avatarBtns = this.$avatarForm.find('.avatar-btns');

            this.$avatarWrapper = this.$avatarModal.find('.avatar-wrapper');
            this.$avatarPreview = this.$avatarModal.find('.avatar-preview');

            this.init();
            console.log(this);
        }

        CropAvatar.prototype = {
            constructor: CropAvatar,

            support: {
                fileList: !!$('<input type="file">').prop('files'),
                blobURLs: !!window.URL && URL.createObjectURL,
                formData: !!window.FormData
            },

            init: function () {
                this.support.datauri = this.support.fileList && this.support.blobURLs;

                if (!this.support.formData) {
                    this.initIframe();
                }

                this.initTooltip();
                this.initModal();
                this.addListener();

            },

            addListener: function () {
                this.$avatarView.on('click', $.proxy(this.click, this));
                this.$avatarInput.on('change', $.proxy(this.change, this));
                this.$avatarForm.on('submit', $.proxy(this.submit, this));
                this.$avatarBtns.on('click', $.proxy(this.rotate, this));
            },

            initTooltip: function () {
                this.$avatarView.tooltip({
                    placement: 'bottom'
                });
            },

            initModal: function () {
                //this.$avatarModal.show();
                this.$avatarModal.modal({ show: false });
            },

            initPreview: function () {
                var url = this.$avatar.attr('src');
                this.$avatarPreview.empty().html('<img src="' + url + '">');

            },

            initIframe: function () {
                var target = 'upload-iframe-' + (new Date()).getTime(),
          $iframe = $('<iframe>').attr({
              name: target,
              src: ''
          }),
          _this = this;

                // Ready ifrmae
                $iframe.one('load', function () {

                    // respond response
                    $iframe.on('load', function () {
                        var data;

                        try {
                            data = $(this).contents().find('body').text();
                        } catch (e) {
                            console.log(e.message);
                        }

                        if (data) {
                            try {
                                data = $.parseJSON(data);
                            } catch (e) {
                                console.log(e.message);
                            }

                            _this.submitDone(data);
                        } else {
                            _this.submitFail('Image upload failed!');
                        }

                        _this.submitEnd();

                    });
                });

                this.$iframe = $iframe;
                this.$avatarForm.attr('target', target).after($iframe.hide());
            },

            click: function () {
                //this.$avatarModal.modal('show');//渐变
                this.initPreview();
                this.url = this.$avatar.attr('src');
                var _this = this;
                var imgSrc = _this.url;
                _this.confirmStartCropper();

            },

            change: function () {//改变上传文件
                var files, file;
                var _this = this;
                _this.alertClose();

                if (!this.support.datauri) { //不支持浏览本地图片
                    file = this.$avatarInput.val();

                    if (this.isImageFile(file)) {
                        this.syncUpload();
                    }
                    HasImg = false;
                    return;
                }
                files = this.$avatarInput.prop('files');
                if (files.length > 0) {
                    file = files[0];

                    if (this.isImageFile(file)) {
                        if (this.url) {
                            URL.revokeObjectURL(this.url); // Revoke the old one
                        }
                        this.url = URL.createObjectURL(file);
                        HasImg = true;
                        this.$avatarSrc.val(""); //必须清空
                        this.confirmStartCropper();
                    }
                }
            },

            submit: function () {
                if (!this.$avatarSrc.val() && !this.$avatarInput.val()) {
                    return false;
                }

                if (this.support.formData) {
                    this.ajaxUpload();
                    return false;
                }
            },

            rotate: function (e) {
                var data;

                if (this.active) {
                    data = $(e.target).data();

                    if (data.method) {
                        this.$img.cropper(data.method, data.option);
                    }
                }
            },

            isImageFile: function (file) {
                if (file.type) {
                    return /^image\/\w+$/.test(file.type);
                } else {
                    return /\.(jpg|jpeg|png|gif)$/.test(file);
                }
            },

            startCropper: function () {
                var _this = this;
                if (!HasImg) {
                } else if (1 == 0 && _this.active) {
                    _this.$img.cropper('replace', _this.url); //直接替换
                } else {

                    _this.$img = $('<img src="' + _this.url + '">');
                    _this.$avatarWrapper.empty().html(_this.$img);
                    _this.$img.cropper({
                        aspectRatio: 1.0 * ImgWidth / ImgHeight,
                        preview: _this.$avatarPreview.selector,
                        strict: true, //如果是true就限制在图片边缘内
                        crop: function (data) {
                            var json = ['{"x":' + data.x, '"y":' + data.y, '"height":' + data.height, '"width":' + data.width, '"to_width":"' + ImgWidth + '"', '"to_height":"' + ImgHeight + '"', '"rotate":' + data.rotate + '}'].join();
                            _this.$avatarData.val(json);
                        }
                    });

                    _this.active = true;

                }
            },

            stopCropper: function () {
                if (this.active) {
                    this.$img.cropper('destroy');
                    this.$img.remove();
                    this.active = false;
                }
            },

            ajaxUpload: function () {
                var url = this.$avatarForm.attr('action');
                var data = new FormData(this.$avatarForm[0]);
                var _this = this;
                _this.alertClose();
                $.ajax(url, {
                    type: 'post',
                    data: data,
                    dataType: 'json',
                    processData: false,
                    contentType: false,

                    beforeSend: function () {
                        _this.submitStart();
                    },

                    success: function (data) {
                        _this.submitDone(data);
                    },

                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        _this.submitFail(textStatus || errorThrown);
                    },

                    complete: function () {
                        _this.submitEnd();
                    }
                });
            },

            syncUpload: function () {
                this.$avatarSave.click();
            },

            submitStart: function () {
                this.$loading.fadeIn();
            },

            submitDone: function (data) {
                console.log(data);

                if ($.isPlainObject(data) && data.state === 200) {
                    if (data.result) {
                        this.url = data.result;

                        if (this.support.datauri || this.uploaded) {
                            this.uploaded = false;
                            this.cropDone();
                        } else {
                            this.uploaded = true;
                            this.$avatarSrc.val(this.url); //传的是一个url地址
                            this.confirmStartCropper();
                        }

                        this.$avatarInput.val('');
                    } else if (data.message) {
                        this.alert(data.message);
                    }
                } else {
                    this.alert('Failed to response');
                }
            },

            submitFail: function (msg) {
                this.alert(msg);
            },

            submitEnd: function () {
                this.$loading.fadeOut();
            },

            cropDone: function () {
                try {
                    parent.FillPhoto(this.url);
                    parent.JumboTCMS.Popup.hide();
                } catch (e) {
                    console.log(e.message);
                    this.$avatarForm.get(0).reset();
                    this.$avatar.attr('src', this.url);
                    this.stopCropper();
                    //this.$avatarModal.modal('hide');
                }

            },

            alert: function (msg) {
                var $alert = [
            '<div class="alert alert-danger avater-alert">',
              '<button type="button" class="close" data-dismiss="alert">&times;</button>',
              msg,
            '</div>'
          ].join('');
                var _this = this;
                _this.alertClose();
                _this.$avatarUpload.after($alert);
            },
            alertClose: function () {

                $('.avater-alert').remove();
            },

            confirmStartCropper: function () {
                var _this = this;
                getImageWidth(_this.url, function (w, h) {
                    if (w < ImgWidth || h < ImgHeight) {
                        //_this.alert("图片尺寸不得小于" + ImgWidth + "px * " + ImgHeight + "px");
                        //_this.$avatarPreview.empty();
                        //_this.stopCropper();

                    }
                    //else 
                    {
                        _this.startCropper();
                    }
                });
            }
        };

        $(function () {
            var _this_cropavatar = new CropAvatar($('#crop-avatar'));
            _this_cropavatar.click();
        });

    });


    // 获取图片真实高度
    function getImageWidth(url, callback) {
        var img = new Image();
        img.src = url;
        // 如果图片被缓存，则直接返回缓存数据
        if (img.complete) {
            callback(img.width, img.height);
        } else {
            img.onload = function () {
                callback(img.width, img.height);
            }
        }
    }
</script>
</body>
</html>
