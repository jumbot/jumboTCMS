﻿/*
 * 程序名称: JumboTCMS(将博内容管理系统通用版)
 * 
 * 程序版本: 7.x
 * 
 * 程序作者: 子木将博 (QQ：791104444@qq.com，仅限商业合作)
 * 
 * 版权申明: http://www.jumbotcms.net/about/copyright.html
 * 
 * 技术答疑: http://forum.jumbotcms.net/
 * 
 */


using System;
using JumboTCMS.Common;
using Newtonsoft.Json.Linq;
namespace JumboTCMS.WebFile.Plus
{
    public partial class Ajax_User : JumboTCMS.UI.UserCenter
    {
        private string _operType = string.Empty;
        private string _response = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            ChannelId = Str2Str(q("ChannelId"));
            this._operType = q("oper");
            switch (this._operType)
            {
                case "ajaxConfirmDownload":
                    ajaxConfirmDownload();
                    break;
                default:
                    DefaultResponse();
                    break;
            }
            Response.Write(this._response);
        }
        private void DefaultResponse()
        {
            User_Load("", "json",true);
        }

        #region  下载相关
        /// <summary>
        /// 确认下载条件
        /// </summary>
        private void ajaxConfirmDownload()
        {
            User_Load("", "json", true);
            id = Str2Str(q("id"));
            ChannelId = Str2Str(q("ChannelId"));
            ChannelType = q("ChannelType");
            string _modulelist2 = JumboTCMS.Utils.XmlCOM.ReadConfig("~/_data/config/site", "ModuleList2");
            if (!_modulelist2.Contains("." + ChannelType + "."))
            {
                this._response = JsonResult(-1, "请不要恶意修改提交参数!");
                return;
            }
            int NO = Str2Int(q("NO"));

            doh.Reset();
            doh.ConditionExpress = "ChannelId=" + ChannelId + " and id=" + id;
            object[] _obj = doh.GetFields("jcms_module_" + ChannelType, "Title,Points," + ChannelType + "Url");
            if (_obj == null)
            {
                this._response = JsonResult(-1, "id参数有误!");
                return;
            }
            string _SourceTitle = _obj[0].ToString();
            int _Points = Str2Int(_obj[1].ToString(), 0);
            string downUrl = _obj[2].ToString().Replace("\r\n", "\r");
            if (downUrl == "")
            {
                this._response = JsonResult(-1, "当前下载地址为空!");
                return;
            }

            if (_Points > 0)//说明是需要扣除将元的，那么肯定要判断当前用户将元够不够
            {
                if (!CanDownFile(ChannelType, ChannelId, id, _Points, _SourceTitle))
                {
                    this._response = JsonResult(-1, "您账户的余额不足" + _Points + "将元!");
                    return;
                }
            }
            doh.Reset();
            doh.ConditionExpress = "ChannelId=" + ChannelId + " and id=" + id;
            if (JumboTCMS.Utils.Cookie.GetValue(site.CookiePrev + "admin") == null)
                doh.ConditionExpress += " AND [IsPass]=1";
            doh.Add("jcms_module_" + ChannelType, "DownNum");
            string[] _DownUrl = downUrl.Split(new string[] { "\r" }, StringSplitOptions.None);
            if ((NO > _DownUrl.Length - 1) || NO < 0)
            {
                this._response = JsonResult(-1, "请不要恶意修改提交参数!");
                return;
            }
            string _url = _DownUrl[NO];
            if (_url.Contains("|||"))
                _url = _url.Substring(_url.IndexOf("|||") + 3, (_url.Length - _url.IndexOf("|||") - 3));
            if (JumboTCMS.Utils.DirFile.FileExists(_url))
            {
                JObject jo = new JObject();
                jo.Add("title", _SourceTitle);
                jo.Add("filename", _url);
                jo.Add("md5", JumboTCMS.Utils.MD5.Upper32(_url + "_" + _SourceTitle));
                this._response = JsonResult(1, "ok", jo);
            }
            else
                this._response = JsonResult(-1, "下载文件不存在!");
        }

        /// <summary>
        /// 判断附件下载权限
        /// 并扣除相应的将元
        /// </summary>
        /// <param name="_ChannelType"></param>
        /// <param name="_ChannelId">频道ID</param>
        /// <param name="_SourceId">内容ID</param>
        /// <param name="_Points">扣除将元</param>
        /// <param name="_SourceTitle">资源名称</param>
        /// <returns></returns>
        private bool CanDownFile(string _ChannelType, string _ChannelId, string _SourceId, int _Points, string _SourceTitle)
        {
            bool _isvip = CurrentUser.IsVIP == 1;
            if (!_isvip)//给用户扣除将元,VIP不扣除
            {
                doh.Reset();
                doh.ConditionExpress = "ChannelId=" + _ChannelId + " and [" + _ChannelType + "Id]=" + _SourceId + " and UserId=" + UserId;
                if (doh.Exist("jcms_module_" + _ChannelType + "_downlogs"))//说明已经扣过
                {
                    return true;
                }
                doh.Reset();
                doh.ConditionExpress = "id=" + UserId;
                int _myPoints = Str2Int(doh.GetField("jcms_normal_user", "Points").ToString());
                if (_myPoints < _Points)//说明将元不够
                    return false;
                //扣除将元
                new JumboTCMS.DAL.Normal_UserDAL().DeductPoints(UserId, _Points);
                string _OperInfo1 = "下载资源:<a href=\"" + Go2View(1, false, _ChannelId, _SourceId, false) + "\" target=\"_blank\">" + _SourceTitle + "</a>，扣除了" + _Points + "将元";
                new JumboTCMS.DAL.Normal_UserLogsDAL().SaveLog(UserId, _OperInfo1, 2);
                //增加一个下载日志记录
                doh.Reset();
                doh.AddFieldItem("UserId", UserId);
                doh.AddFieldItem("ChannelId", _ChannelId);
                doh.AddFieldItem(_ChannelType + "Id", _SourceId);
                doh.AddFieldItem("Points", _Points);
                doh.AddFieldItem("DownTime", DateTime.Now.ToString());
                doh.AddFieldItem("DownIP", Const.GetUserIp);
                doh.AddFieldItem("DownDegree", 1);
                doh.Insert("jcms_module_" + _ChannelType + "_downlogs");
            }
            return true;
        }
        #endregion

    }
}