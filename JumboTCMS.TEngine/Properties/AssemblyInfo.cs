﻿/*JumboTCMS作者申明
 * 该项目已经在原来的开源项目Ader TemplateEngine上改动，为避免冲突，特此更改了项目名
 */
using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("Ader TemplateEngine开源项目修改版")]
[assembly: AssemblyDescription("该项目已经在原来的开源项目Ader TemplateEngine上改动，为避免冲突，特此更改了项目名")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("北京子木将博科技有限公司")]
[assembly: AssemblyProduct("JumboTCMS.TEngine")]
[assembly: AssemblyCopyright("www.jumbot.net")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyVersion("7.3.1.0217")]
