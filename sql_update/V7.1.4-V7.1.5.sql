-------V7.1.5
CREATE TABLE [dbo].[jcms_oauth2_app](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AppKey] [nvarchar](64) NOT NULL DEFAULT (''),
	[AppSecret] [nvarchar](64) NOT NULL DEFAULT (''),
	[CallbackURI] [nvarchar](600) NULL,
	[State] [int] NOT NULL DEFAULT ((1)),
	[AppImg] [nvarchar](255) NULL,
	[SiteName] [nvarchar](20) NULL,
	[SiteUrl] [nvarchar](150) NULL,
	[SiteInfo] [nvarchar](255) NULL,
 CONSTRAINT [PK_oauth2_app] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [jcms_oauth2_app] ([AppKey], [AppSecret], [CallbackURI], [State], [AppImg], [SiteName], [SiteUrl], [SiteInfo]) VALUES ('10010100', 'f2fed6cc718f32c25ababd0db7328d7d', 'http://www.jumbotcms.net/app/jumbot/', 1, NULL, '����CMSͨ�ð�', 'http://www.jumbotcms.net', NULL)
GO


