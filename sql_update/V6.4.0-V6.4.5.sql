ALTER TABLE [jcms_module_article] ADD [AddTime] [datetime] NOT NULL DEFAULT (getdate())
GO
update [jcms_module_article] SET [AddTime]=[AddDate]
GO
ALTER TABLE [jcms_module_document] ADD [AddTime] [datetime] NOT NULL DEFAULT (getdate())
GO
update [jcms_module_document] SET [AddTime]=[AddDate]
GO
ALTER TABLE [jcms_module_paper] ADD [AddTime] [datetime] NOT NULL DEFAULT (getdate())
GO
update [jcms_module_paper] SET [AddTime]=[AddDate]
GO
ALTER TABLE [jcms_module_photo] ADD [AddTime] [datetime] NOT NULL DEFAULT (getdate())
GO
update [jcms_module_photo] SET [AddTime]=[AddDate]
GO
ALTER TABLE [jcms_module_product] ADD [AddTime] [datetime] NOT NULL DEFAULT (getdate())
GO
update [jcms_module_product] SET [AddTime]=[AddDate]
GO
ALTER TABLE [jcms_module_soft] ADD [AddTime] [datetime] NOT NULL DEFAULT (getdate())
GO
update [jcms_module_soft] SET [AddTime]=[AddDate]
GO
ALTER TABLE [jcms_module_video] ADD [AddTime] [datetime] NOT NULL DEFAULT (getdate())
GO
update [jcms_module_video] SET [AddTime]=[AddDate]
GO