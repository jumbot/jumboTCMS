ALTER TABLE [jcms_normal_user] add [Token_Tencent] [nvarchar] (32) NOT NULL DEFAULT ('')
GO
ALTER TABLE [jcms_normal_user] add [Token_Sina] [nvarchar] (32) NOT NULL DEFAULT ('')
GO
ALTER TABLE [jcms_normal_user] add [Token_Kaixin] [nvarchar] (32) NOT NULL DEFAULT ('')
GO
ALTER TABLE [jcms_normal_user] add [Token_Baidu] [nvarchar] (32) NOT NULL DEFAULT ('')
GO
ALTER TABLE [jcms_normal_user] add [Token_Renren] [nvarchar] (32) NOT NULL DEFAULT ('')
GO



CREATE TABLE [dbo].[jcms_normal_question] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[ParentId] [int] NOT NULL DEFAULT(0) ,
	[AddDate] [datetime] NULL ,
	[Title] [nvarchar] (50) NOT NULL DEFAULT ('') ,
	[Content] [nvarchar] (250) NOT NULL DEFAULT ('') ,
	[IP] [varchar] (15) NOT NULL DEFAULT ('') ,
	[UserName] [nvarchar] (50) NOT NULL DEFAULT ('') ,
	[UserId] [int] NOT NULL DEFAULT(0) ,
	[ClassId] [int] NOT NULL DEFAULT(0) ,
	[IsPass] [int] NOT NULL DEFAULT(0) ,
	CONSTRAINT [PK_normal_question] PRIMARY KEY CLUSTERED 
	(
		[Id]
	) ON [PRIMARY] 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[jcms_normal_question_class] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[Title] [nvarchar] (50) NOT NULL DEFAULT ('') ,
	[PId] [int] NOT NULL DEFAULT(0) ,
	CONSTRAINT [PK_normal_question_class] PRIMARY KEY CLUSTERED 
	(
		[Id]
	) ON [PRIMARY] 
) ON [PRIMARY]
GO

INSERT INTO [jcms_normal_question_class] ([Title],[PId]) VALUES ('技术咨询',1)
GO
INSERT INTO [jcms_normal_question_class] ([Title],[PId]) VALUES ('产品咨询',2)
GO
INSERT INTO [jcms_normal_question_class] ([Title],[PId]) VALUES ('意见建议',3)
GO
INSERT INTO [jcms_normal_question_class] ([Title],[PId]) VALUES ('其他问题',4)
GO

CREATE TABLE [dbo].[jcms_normal_link] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[Title] [nvarchar] (50) NOT NULL DEFAULT ('') ,
	[Url] [nvarchar] (150) NOT NULL DEFAULT ('') ,
	[ImgPath] [nvarchar] (150) NOT NULL DEFAULT ('') ,
	[Info] [nvarchar] (250) NOT NULL DEFAULT ('') ,
	[OrderNum] [int] NOT NULL DEFAULT(0) ,
	[State] [int] NOT NULL DEFAULT(0) ,
	[Style] [int] NOT NULL DEFAULT(0) ,
	CONSTRAINT [PK_normal_link] PRIMARY KEY CLUSTERED 
	(
		[Id]
	) ON [PRIMARY] 
) ON [PRIMARY]
GO



CREATE TABLE [dbo].[jcms_normal_language] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[Title] [nvarchar] (20) NOT NULL DEFAULT ('') ,
	[Code] [nvarchar] (20) NOT NULL DEFAULT ('cn') ,
	CONSTRAINT [PK_normal_language] PRIMARY KEY CLUSTERED 
	(
		[Id]
	) ON [PRIMARY] 
) ON [PRIMARY]
GO
INSERT INTO [jcms_normal_language] ([Title],[Code]) VALUES ('中文','cn')
GO
INSERT INTO [jcms_normal_language] ([Title],[Code]) VALUES ('英文','en')
GO
ALTER TABLE [jcms_normal_channel] add [LanguageCode] [nvarchar] (20) NOT NULL DEFAULT ('cn')
GO


-------2012-03-20增加

CREATE TABLE [dbo].[jcms_normal_user_oauth] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[Title] [nvarchar] (20) NOT NULL DEFAULT ('') ,
	[Code] [nvarchar] (20) NOT NULL DEFAULT ('') ,
	[PId] [int] NOT NULL DEFAULT(0) ,
	[Enabled] [int] NOT NULL DEFAULT(1) ,
	CONSTRAINT [PK_normal_user_oauth] PRIMARY KEY CLUSTERED 
	(
		[Id]
	) ON [PRIMARY] 
) ON [PRIMARY]
GO

INSERT INTO [jcms_normal_user_oauth] ([Title],[Code],PId,Enabled) VALUES ('新浪微博','sina',1,0)
GO
INSERT INTO [jcms_normal_user_oauth] ([Title],[Code],PId,Enabled) VALUES ('QQ账号','tencent',2,0)
GO
INSERT INTO [jcms_normal_user_oauth] ([Title],[Code],PId,Enabled) VALUES ('人人网账号','renren',3,0)
GO
INSERT INTO [jcms_normal_user_oauth] ([Title],[Code],PId,Enabled) VALUES ('百度账号','baidu',4,0)
GO
INSERT INTO [jcms_normal_user_oauth] ([Title],[Code],PId,Enabled) VALUES ('开心网账号','kaixin',5,0)
GO
