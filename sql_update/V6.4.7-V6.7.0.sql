-------V6.6.0

ALTER TABLE [jcms_normal_channel] ADD [IsIndex] [int] NOT NULL DEFAULT(1)
GO


-------V6.7.0
ALTER TABLE [jcms_normal_link] ADD [ChannelId] [int] NOT NULL DEFAULT(0)
GO

ALTER TABLE [jcms_extends_vote] ADD [ChannelId] [int] NOT NULL DEFAULT(0)
GO