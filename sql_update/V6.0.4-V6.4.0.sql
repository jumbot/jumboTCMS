ALTER TABLE [jcms_module_article] ADD [CollItemID] [int] NOT NULL DEFAULT(0)
GO

if exists (select * from sysobjects where id = OBJECT_ID('[jcms_module_article_collfilters]') and OBJECTPROPERTY(id, 'IsUserTable') = 1) 
DROP TABLE [jcms_module_article_collfilters]
GO
CREATE TABLE [jcms_module_article_collfilters] (
[Id] [int]  IDENTITY (1, 1)  NOT NULL,
[ChannelId] [int]  NOT NULL DEFAULT (0),
[ItemId] [int]  NOT NULL DEFAULT (0),
[Title] [nvarchar]  (50) NULL,
[Filter_Object] [int]  NOT NULL DEFAULT (0),
[Filter_Type] [int]  NOT NULL DEFAULT (0),
[Filter_Content] [ntext]  NULL,
[FisString] [ntext]  NULL,
[FioString] [ntext]  NULL,
[Filter_Rep] [ntext]  NULL,
[Flag] [int]  NOT NULL DEFAULT (0),
[PublicTf] [int]  NOT NULL DEFAULT (0))
GO
ALTER TABLE [jcms_module_article_collfilters] WITH NOCHECK ADD  CONSTRAINT [PK_jcms_module_article_collfilters] PRIMARY KEY  NONCLUSTERED ( [Id] )
GO


if exists (select * from sysobjects where id = OBJECT_ID('[jcms_module_article_colllogs]') and OBJECTPROPERTY(id, 'IsUserTable') = 1) 
DROP TABLE [jcms_module_article_colllogs]
GO
CREATE TABLE [jcms_module_article_colllogs] (
[Id] [int]  IDENTITY (1, 1)  NOT NULL,
[ItemId] [int]  NOT NULL,
[AdminName] [nvarchar]  (30) NOT NULL DEFAULT (''),
[CollectInfo] [nvarchar]  (100) NOT NULL DEFAULT (''),
[StartTime] [datetime]  NOT NULL DEFAULT (getdate()),
[EndTime] [datetime]  NOT NULL DEFAULT (getdate()),
[CollectIP] [nvarchar]  (15) NULL)
GO
ALTER TABLE [jcms_module_article_colllogs] WITH NOCHECK ADD  CONSTRAINT [PK_jcms_module_article_colllogs] PRIMARY KEY  NONCLUSTERED ( [Id] )
GO



if exists (select * from sysobjects where id = OBJECT_ID('[jcms_module_article_collitem]') and OBJECTPROPERTY(id, 'IsUserTable') = 1) 
DROP TABLE [jcms_module_article_collitem]
GO
CREATE TABLE [jcms_module_article_collitem] (
[Id] [int]  IDENTITY (1, 1)  NOT NULL,
[Title] [nvarchar]  (50) NOT NULL,
[ChannelId] [int]  NOT NULL DEFAULT (0),
[ClassId] [int]  NOT NULL DEFAULT (0),
[State] [int]  NOT NULL DEFAULT (0),
[WebUrl] [nvarchar]  (100) NULL,
[ItemDemo] [nvarchar]  (200) NULL,
[ListStr] [nvarchar]  (200) NULL,
[ListStart] [ntext]  NULL,
[ListEnd] [ntext]  NULL,
[LinkStart] [ntext]  NULL,
[LinkEnd] [ntext]  NULL,
[TitleStart] [ntext]  NULL,
[TitleEnd] [ntext]  NULL,
[ContentStart] [ntext]  NULL,
[ContentEnd] [ntext]  NULL,
[WebName] [nvarchar]  (50) NULL,
[SourceFromStart] [nvarchar]  (300) NULL,
[SourceFromEnd] [nvarchar]  (300) NULL,
[PubTimeStart] [nvarchar]  (300) NULL,
[PubTimeEnd] [nvarchar]  (300) NULL,
[NPageStart] [nvarchar]  (300) NULL,
[NPageEnd] [nvarchar]  (300) NULL,
[AuthorStr] [nvarchar]  (50) NULL,
[Flag] [int]  NOT NULL DEFAULT (0),
[Script_Iframe] [int]  NOT NULL DEFAULT (0),
[Script_Object] [int]  NOT NULL DEFAULT (0),
[Script_Script] [int]  NOT NULL DEFAULT (0),
[Script_Div] [int]  NOT NULL DEFAULT (0),
[Script_Table] [int]  NOT NULL DEFAULT (0),
[Script_Span] [int]  NOT NULL DEFAULT (0),
[Script_Img] [int]  NOT NULL DEFAULT (0),
[Script_Font] [int]  NOT NULL DEFAULT (0),
[Script_A] [int]  NOT NULL DEFAULT (0),
[Script_Html] [int]  NOT NULL DEFAULT (0),
[CollecNewsNum] [int]  NOT NULL DEFAULT (0),
[SaveFiles] [int]  NOT NULL DEFAULT (0),
[CollecOrder] [int]  NOT NULL DEFAULT (0),
[ListWebEncode] [nvarchar]  (50) NULL,
[ContentWebEncode] [nvarchar]  (50) NULL,
[LastTime] [datetime]  NOT NULL DEFAULT (getdate()),
[AutoChecked] [int]  NOT NULL DEFAULT (0),
[IsRunning] [int]  NOT NULL DEFAULT (0),
[Running] [int]  NOT NULL DEFAULT (0),
[AutoCollect] [int]  NOT NULL DEFAULT (1),
[AutoCollectHours] [nvarchar]  (100) NOT NULL DEFAULT (',2,3,8,9,14,15,20,21,'),
[AutoCollectNextTime] [datetime]  NOT NULL DEFAULT (getdate()),
[AutoCollectNum] [int]  NOT NULL DEFAULT (20),
[LastListHTML] [nvarchar]  (MAX) NOT NULL DEFAULT (''),
[LinkBaseHref] [nvarchar]  (200) NOT NULL DEFAULT (''),
[AutoChecked2] [int]  NOT NULL DEFAULT (0),
[CollecOrder2] [int]  NOT NULL DEFAULT (0),
[SourceFrom] [nvarchar]  (30) NOT NULL DEFAULT (''),
[SaveFiles2] [int]  NOT NULL DEFAULT (0),
[ErrorListRule] [int]  NOT NULL DEFAULT (0),
[ErrorPageRule] [int]  NOT NULL DEFAULT (0),
[Deleted] [int]  NOT NULL DEFAULT (0))
GO
ALTER TABLE [jcms_module_article_collitem] WITH NOCHECK ADD  CONSTRAINT [PK_jcms_module_article_collitem] PRIMARY KEY  NONCLUSTERED ( [Id] )
GO


if exists (select * from sysobjects where id = OBJECT_ID('[jcms_module_article_collhistory]') and OBJECTPROPERTY(id, 'IsUserTable') = 1) 
DROP TABLE [jcms_module_article_collhistory]
GO
CREATE TABLE [jcms_module_article_collhistory] (
[Id] [bigint]  IDENTITY (1, 1)  NOT NULL,
[ChannelId] [int]  NOT NULL DEFAULT (0),
[ItemId] [int]  NOT NULL DEFAULT (0),
[Title] [nvarchar]  (100) NULL,
[CollectDate] [datetime]  NOT NULL DEFAULT (getdate()),
[NewsUrl] [nvarchar]  (200) NULL,
[ResultStr] [nvarchar]  (200) NULL,
[Result] [int]  NOT NULL DEFAULT (0))
GO
ALTER TABLE [jcms_module_article_collhistory] WITH NOCHECK ADD  CONSTRAINT [PK_jcms_module_article_collhistory] PRIMARY KEY  NONCLUSTERED ( [Id] )
GO