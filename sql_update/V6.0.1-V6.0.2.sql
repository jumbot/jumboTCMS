ALTER TABLE [jcms_module_article] add [IsHead] [int] NOT NULL DEFAULT(0)
GO
ALTER TABLE [jcms_module_photo] add [IsHead] [int] NOT NULL DEFAULT(0)
GO
ALTER TABLE [jcms_module_video] add [IsHead] [int] NOT NULL DEFAULT(0)
GO
ALTER TABLE [jcms_module_soft] add [IsHead] [int] NOT NULL DEFAULT(0)
GO
ALTER TABLE [jcms_module_document] add [IsHead] [int] NOT NULL DEFAULT(0)
GO
ALTER TABLE [jcms_module_paper] add [IsHead] [int] NOT NULL DEFAULT(0)
GO
ALTER TABLE [jcms_module_product] add [IsHead] [int] NOT NULL DEFAULT(0)
GO

ALTER TABLE [jcms_normal_class] add [IsPaging] [int] NOT NULL DEFAULT(0)
GO