ALTER TABLE [jcms_normal_channel] add [IsTop] [int] NOT NULL DEFAULT(0)
GO
ALTER TABLE [jcms_normal_class] add [FirstPage] [nvarchar] (150) NOT NULL DEFAULT ('')
GO
UPDATE [jcms_normal_class] SET [FirstPage]=lTrim(OutUrl) WHERE [outurl] is NOT null
GO
ALTER TABLE [jcms_module_article] add [FirstPage] [nvarchar] (150) NOT NULL DEFAULT ('')
GO
UPDATE [jcms_module_article] SET [FirstPage]=lTrim(OutUrl) WHERE [outurl] is NOT null
GO
ALTER TABLE [jcms_module_photo] add [FirstPage] [nvarchar] (150) NOT NULL DEFAULT ('')
GO
UPDATE [jcms_module_photo] SET [FirstPage]=lTrim(OutUrl) WHERE [outurl] is NOT null
GO
ALTER TABLE [jcms_module_product] add [FirstPage] [nvarchar] (150) NOT NULL DEFAULT ('')
GO
UPDATE [jcms_module_product] SET [FirstPage]=lTrim(OutUrl) WHERE [outurl] is NOT null
GO
ALTER TABLE [jcms_module_soft] add [FirstPage] [nvarchar] (150) NOT NULL DEFAULT ('')
GO
UPDATE [jcms_module_soft] SET [FirstPage]=lTrim(OutUrl) WHERE [outurl] is NOT null
GO
ALTER TABLE [jcms_module_video] add [FirstPage] [nvarchar] (150) NOT NULL DEFAULT ('')
GO
UPDATE [jcms_module_video] SET [FirstPage]=lTrim(OutUrl) WHERE [outurl] is NOT null
GO
ALTER TABLE [jcms_module_document] add [FirstPage] [nvarchar] (150) NOT NULL DEFAULT ('')
GO
UPDATE [jcms_module_document] SET [FirstPage]=lTrim(OutUrl) WHERE [outurl] is NOT null
GO
ALTER TABLE [jcms_module_paper] add [FirstPage] [nvarchar] (150) NOT NULL DEFAULT ('')
GO
UPDATE [jcms_module_paper] SET [FirstPage]=lTrim(OutUrl) WHERE [outurl] is NOT null
GO


ALTER TABLE [jcms_normal_class] add [AliasPage] [nvarchar] (150) NOT NULL DEFAULT ('')
GO
ALTER TABLE [jcms_module_article] add [AliasPage] [nvarchar] (150) NOT NULL DEFAULT ('')
GO
ALTER TABLE [jcms_module_photo] add [AliasPage] [nvarchar] (150) NOT NULL DEFAULT ('')
GO
ALTER TABLE [jcms_module_product] add [AliasPage] [nvarchar] (150) NOT NULL DEFAULT ('')
GO
ALTER TABLE [jcms_module_soft] add [AliasPage] [nvarchar] (150) NOT NULL DEFAULT ('')
GO
ALTER TABLE [jcms_module_video] add [AliasPage] [nvarchar] (150) NOT NULL DEFAULT ('')
GO
ALTER TABLE [jcms_module_document] add [AliasPage] [nvarchar] (150) NOT NULL DEFAULT ('')
GO
ALTER TABLE [jcms_module_paper] add [AliasPage] [nvarchar] (150) NOT NULL DEFAULT ('')
GO