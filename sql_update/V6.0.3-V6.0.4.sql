ALTER TABLE [jcms_normal_class] add [Content] [ntext] NOT NULL DEFAULT ('')
GO

ALTER TABLE [jcms_normal_class] add [Keywords] [nvarchar] (100) NOT NULL DEFAULT ('')
GO

ALTER TABLE [jcms_normal_channel] add [IsPaging] [int] NOT NULL DEFAULT(0)
GO

ALTER TABLE [jcms_normal_channel] add [PageSize] [int] NOT NULL DEFAULT(20)
GO