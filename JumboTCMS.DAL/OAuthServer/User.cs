﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JumboTCMS.DAL.OAuthServer
{
    public class User
    {
        public User() { }

        public User(string _userid, string _email, string _nickname, string _headimgurl)
        {
            userid = _userid;
            email = _email;
            nickname = _nickname;
            headimgurl = _headimgurl;
            valid = true;
        }

        public bool valid = false;
        /// <summary>
        /// 用户id
        /// </summary>
        public string userid { get; set; }
        /// <summary>
        /// 昵称
        /// </summary>
        public string nickname { get; set; }
        /// <summary>
        /// 头像
        /// </summary>
        public string headimgurl { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        public string email { get; set; }
        public DateTime timestamp = DateTime.Now;
    }
}