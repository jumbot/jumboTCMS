﻿/*
 * 程序名称: JumboTCMS(将博内容管理系统通用版)
 * 
 * 程序版本: 7.x
 * 
 * 程序作者: 子木将博 (QQ：791104444@qq.com，仅限商业合作)
 * 
 * 版权申明: http://www.jumbotcms.net/about/copyright.html
 * 
 * 技术答疑: http://forum.jumbotcms.net/
 * 
 */

using System;
using System.Collections.Generic;
using System.Data;
namespace JumboTCMS.Entity
{
    /// <summary>
    /// 标签-------表映射实体
    /// </summary>
    public class Normal_Tags
    {
        public Normal_Tags()
        { }
        public List<Normal_Tag> DT2List(DataTable _dt)
        {
            if (_dt == null) return null;
            return JumboTCMS.Utils.dtHelp.DT2List<Normal_Tag>(_dt);
        }
    }
    public class Normal_Tag
    {
        public Normal_Tag()
        { }

        private string _id;
        private int _channelid;
        private string _title;
        private int _clicktimes;
        /// <summary>
        /// 编号
        /// </summary>
        public string Id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 频道ID
        /// </summary>
        public int ChannelId
        {
            set { _channelid = value; }
            get { return _channelid; }
        }
        /// <summary>
        /// 标签名
        /// </summary>
        public string Title
        {
            set { _title = value; }
            get { return _title; }
        }
        /// <summary>
        /// 点击数
        /// </summary>
        public int ClickTimes
        {
            set { _clicktimes = value; }
            get { return _clicktimes; }
        }


    }
}

