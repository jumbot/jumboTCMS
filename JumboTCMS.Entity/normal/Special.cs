﻿/*
 * 程序名称: JumboTCMS(将博内容管理系统通用版)
 * 
 * 程序版本: 7.x
 * 
 * 程序作者: 子木将博 (QQ：791104444@qq.com，仅限商业合作)
 * 
 * 版权申明: http://www.jumbotcms.net/about/copyright.html
 * 
 * 技术答疑: http://forum.jumbotcms.net/
 * 
 */

using System;
using System.Collections.Generic;
using System.Data;
namespace JumboTCMS.Entity
{
    /// <summary>
    /// 专题-------表映射实体
    /// </summary>
    public class Normal_Specials
    {
        public Normal_Specials()
        { }
        public List<Normal_Special> DT2List(DataTable _dt)
        {
            if (_dt == null) return null;
            return JumboTCMS.Utils.dtHelp.DT2List<Normal_Special>(_dt);
        }
    }
    public class Normal_Special
    {
        public Normal_Special()
        { }
        private string _id;
        private string _title;
        private string _info;
        private string _source;
        /// <summary>
        /// 编号
        /// </summary>
        public string Id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 专题标题
        /// </summary>
        public string Title
        {
            set { _title = value; }
            get { return _title; }
        }
        /// <summary>
        /// 专题简介
        /// </summary>
        public string Info
        {
            set { _info = value; }
            get { return _info; }
        }
        /// <summary>
        /// 专题文件名
        /// </summary>
        public string Source
        {
            set { _source = value; }
            get { return _source; }
        }


    }
}

