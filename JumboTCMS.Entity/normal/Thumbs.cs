﻿/*
 * 程序名称: JumboTCMS(将博内容管理系统通用版)
 * 
 * 程序版本: 7.x
 * 
 * 程序作者: 子木将博 (QQ：791104444@qq.com，仅限商业合作)
 * 
 * 版权申明: http://www.jumbotcms.net/about/copyright.html
 * 
 * 技术答疑: http://forum.jumbotcms.net/
 * 
 */

using System;
namespace JumboTCMS.Entity
{
    /// <summary>
    /// 缩略图尺寸-------表映射实体
    /// </summary>

    public class Normal_Thumbs
    {
        public Normal_Thumbs()
        { }

        private string _id;
        private int _channelid;
        private string _title;
        private int _iwidth;
        private int _iheight;
        /// <summary>
        /// 
        /// </summary>
        public string Id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int ChannelId
        {
            set { _channelid = value; }
            get { return _channelid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Title
        {
            set { _title = value; }
            get { return _title; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int iWidth
        {
            set { _iwidth = value; }
            get { return _iwidth; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int iHeight
        {
            set { _iheight = value; }
            get { return _iheight; }
        }


    }
}

