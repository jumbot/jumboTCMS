﻿/*
 * 程序名称: JumboTCMS(将博内容管理系统通用版)
 * 
 * 程序版本: 7.x
 * 
 * 程序作者: 子木将博 (QQ：791104444@qq.com，仅限商业合作)
 * 
 * 版权申明: http://www.jumbotcms.net/about/copyright.html
 * 
 * 技术答疑: http://forum.jumbotcms.net/
 * 
 */

using System;
namespace JumboTCMS.Entity
{
    /// <summary>
    /// 发信日志-------表映射实体
    /// </summary>

    public class Email_Sendlogs
    {
        public Email_Sendlogs()
        { }

        private string _id;
        private int _adminid;
        private string _sendtitle;
        private string _sendusers;
        private DateTime _sendtime;
        private string _sendip;
        /// <summary>
        /// 
        /// </summary>
        public string Id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int AdminId
        {
            set { _adminid = value; }
            get { return _adminid; }
        }
        /// <summary>
        /// 发信标题
        /// </summary>
        public string SendTitle
        {
            set { _sendtitle = value; }
            get { return _sendtitle; }
        }
        /// <summary>
        /// 发信收件人
        /// </summary>
        public string SendUsers
        {
            set { _sendusers = value; }
            get { return _sendusers; }
        }
        /// <summary>
        /// 发信时间
        /// </summary>
        public DateTime SendTime
        {
            set { _sendtime = value; }
            get { return _sendtime; }
        }
        /// <summary>
        /// 发信IP
        /// </summary>
        public string SendIP
        {
            set { _sendip = value; }
            get { return _sendip; }
        }


    }
}

